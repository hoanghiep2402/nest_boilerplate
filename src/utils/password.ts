import * as bcrypt from 'bcrypt';

export const hash = async (plainPassword: string) => {
  return await bcrypt.hash(plainPassword, 10);
};

export const compare = async (plainPass, hashedPass) => {
  return await bcrypt.compare(plainPass, hashedPass);
};

export default {
  hash,
  compare,
};
