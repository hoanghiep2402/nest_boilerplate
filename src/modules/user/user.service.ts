import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import password from '../../utils/password';
import { PostgresErrorCodesEnum } from '../../database/PostgresErrorCodes.enum';
import { FindConditions } from 'typeorm/find-options/FindConditions';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    try {
      const hashedPassword = await password.hash(createUserDto.password);
      const createdUser = await this.userRepository.save({
        ...createUserDto,
        password: hashedPassword,
      });
      console.log(createdUser);
      return createdUser;
    } catch (e) {
      console.log(e);
      if (e.code === PostgresErrorCodesEnum.duplicated) {
        throw new HttpException(
          'User with that email already exists',
          HttpStatus.BAD_REQUEST,
        );
      }
      throw new HttpException(
        'Something went wrong!',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async findOneById(id: string) {
    const user = await this.userRepository.findOne(id);
    if (user) {
      return user;
    }
    throw new HttpException('User not found', HttpStatus.NOT_FOUND);
  }

  async findOneWhere(conditions: FindConditions<User>): Promise<User> {
    const user = await this.userRepository.findOne({ where: conditions });
    if (!user) {
      return null;
    }
    return user;
  }
}
