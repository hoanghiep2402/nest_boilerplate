import { Column, Entity } from 'typeorm';
import { Exclude } from 'class-transformer';
import { BaseModel } from '../../../database/BaseModel';

@Entity()
export class User extends BaseModel {
  @Column('varchar', {
    length: '100',
  })
  name: string;

  @Column('varchar', {
    length: '50',
    unique: true,
    nullable: false,
  })
  email: string;

  @Column('varchar', {
    length: '20',
    unique: true,
    nullable: false,
  })
  phone: string;

  @Exclude({ toPlainOnly: true })
  @Column('varchar', {
    nullable: false,
  })
  password: string;
}
