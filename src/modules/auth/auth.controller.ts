import {
  Controller,
  Post,
  Body,
  HttpException,
  HttpStatus,
  Get,
  Response,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { UserService } from '../user/user.service';
import password from '../../utils/password';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('/login')
  async login(@Body() loginDto: LoginDto) {
    const user = await this.userService.findOneWhere({ email: loginDto.email });
    if (!user || !(await password.compare(loginDto.password, user.password))) {
      throw new HttpException(
        'Wrong email or password!',
        HttpStatus.BAD_REQUEST,
      );
    }

    const accessToken = this.authService.genToken({ userId: user.id });

    return {
      id: user.id,
      accessToken: accessToken,
    };
  }

  @Get('')
  getUser(@Response({ passthrough: true }) req) {
    return req.locals.user;
  }
}
