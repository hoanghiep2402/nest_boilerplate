import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';

export interface ITokenPayload {
  userId: string;
}

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UserService,
  ) {}
  genToken(payload: ITokenPayload) {
    return this.jwtService.sign({ userId: payload.userId });
  }

  async verifyToken(token: string) {
    try {
      const payload = this.jwtService.verify<ITokenPayload>(token);
      return await this.userService.findOneById(payload.userId);
    } catch (e) {
      console.log(e);
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }
}
