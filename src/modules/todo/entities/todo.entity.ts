import { Column } from 'typeorm';
import { BaseModel } from '../../../database/BaseModel';

export class Todo extends BaseModel {
  @Column('varchar', {
    length: '100',
  })
  description: string;

  @Column('varchar', {
    length: '50',
    unique: true,
    nullable: false,
  })
  deadline: Date;

  @Column('varchar', {
    length: '20',
    unique: true,
    nullable: false,
  })
  status: string;
}
